# encoding: utf8

# Copyright (c) 2012 Martin Raspaud <martin.raspaud@smhi.se>
# Copyright (c) 2015 Nicolas Piaget <nicolas.piaget@env.ethz.ch>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

""" Collections of tools to simplify the use of pytroll
"""
import os
import mpop
from pyproj import Proj
from pyresample import geometry


def coord2area(proj, min_lat, max_lat, min_lon, max_lon, resolution):
    """
    Return the parameters need for an AreaDefinition (pyresample)

    Usage:


    """

    lat_0 = (min_lat + max_lat) / 2.
    lon_0 = (min_lon + max_lon) / 2.

    p = Proj(proj=proj, lat_0=lat_0, lon_0=lon_0, ellps="WGS84")

    x_ur, y_ur = p(max_lon, max_lat)
    x_lr, y_lr = p(max_lon, min_lat)
    x_ll, y_ll = p(min_lon, min_lat)
    x_ul, y_ul = p(min_lon, max_lat)

    area_extent = (min(x_ll, x_ul), min(y_lr, y_ll),
                   max(x_ur, x_lr), max(y_ul, y_ur))
    xsize = int((area_extent[2] - area_extent[0]) / resolution)
    ysize = int((area_extent[3] - area_extent[1]) / resolution)

    return lon_0, lat_0, xsize, ysize, area_extent


def coord2areadef(name, proj, min_lat, max_lat, min_lon, max_lon, resolution):
    """
    Create an AreaDefinition as defined by pyresample

    Usage:

    """

    lon_0, lat_0, xsize, ysize, area_extent = coord2area(proj, min_lat,
                                                         max_lat, min_lon,
                                                         max_lon, resolution)

    proj_id = '{}_{}_{}'.format(proj, lon_0, lat_0)
    proj_dict = {'proj': proj, 'lat_0': lat_0, 'lon_0': lon_0,
                 'ellps': 'WGS84'}

    return geometry.AreaDefinition(name, name, proj_id,
                                   proj_dict, xsize, ysize, area_extent)


def prepare_config(case, severi_path=None, safnw_path=None):
    """
    Write configuration file for the <case> in:
        ${HOME}/.config/pytroll/<case>/
    and set the config path of mpop to this directory
    """

    configdir = '{home}/.config/pytroll/{case}/'
    configdir = configdir.format(home=os.environ['HOME'], case=case)
    mpop.CONFIG_PATH = configdir
