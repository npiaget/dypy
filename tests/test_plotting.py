
import numpy as np
import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt
from PIL import Image
from path import path
from dypy.plotting import Mapfigure
from dypy.lagranto import Tra

testdatadir = path.dirname(path(__file__)).joinpath('test_data')


def test_plot_trajs():
    picfile = testdatadir.joinpath('images_plot_trajs.png')
    if picfile.isfile():
        ctrl_array = np.array(Image.open(picfile))
    filename = testdatadir.joinpath('lsl_lagranto2_0.nc')
    trajs = Tra(filename)

    m = Mapfigure(domain=[0, 15, 30, 55], resolution='l')

    fig, ax = plt.subplots()
    m.ax = ax
    m.drawmap()
    m.plot_traj(trajs, 'Q')
    if picfile.isfile():
        npicfile = picfile.replace(picfile.ext, '_test' + picfile.ext)
        fig.savefig(npicfile)
        test_array = np.array(Image.open(npicfile))
        np.testing.assert_almost_equal(ctrl_array, test_array)
        path(npicfile).remove()
    else:
        fig.savefig(picfile)
