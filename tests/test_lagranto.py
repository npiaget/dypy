from dypy.lagranto import Tra
import glob


def test_read():
    filenames = glob.glob('test_data/lsl_lagranto2_0.*')
    for filename in filenames:
        print(filename)
        trajs = Tra(filename)
        assert type(trajs) == Tra
