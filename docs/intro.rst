Introduction
=============

The aim of the DyPy package is to wrap tools used daily in the group.
The package is divided in several subpackages.

* :ref:`lagranto-package`
  Module to read and write trajectories. Load the trajectories in a numpy array
