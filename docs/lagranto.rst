.. _lagranto-package:

lagranto package
================


Examples
---------

In a first step, let's simply read the trajectories::

    >>> from dypy.lagranto import Tra
    >>> filenme = 'lsl_20110123_10'
    >>> trajs = Tra(filename)

The format of the trajectory file is automatically guessed, but you can define it by using the following command::

    >>> trajs = Tra(filename, typefile='netcdf')

The proprieties of the trajectories can be shown as follow::
    >>> print(trajs)
    24 trajectories with 41 time steps.
    Available fields: time/lon/lat/p/Q/RH/TH/BLH
    total duration: -14400.0 minutes
    >>> print(trajs.variables())
    ['time', 'lon', 'lat', 'p', 'Q', 'RH', 'TH', 'BLH']
    >>> print(trajs['Q'].shape)
    (24, 41)

   
DocStrings
----------

.. autoclass:: dypy.lagranto.Tra
  :members:

