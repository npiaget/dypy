netcdf package
==============

This module is a wrapper for the netCDF4 library.
It aims at shorten the reading of netcdf file.


Examples
--------

Let's start with reading one or several variables::

        >>> import dypy.netcdf as dnc
        >>> filename  = 'foo.nc'

To display the variables of the netcdf file::

        >>> print(dnc.read_variables(filename))
        [u'time', u'lon', u'lat', u'p', u'QV', u'RH', u'TH', u'BLH', u'BASEDATE']

You can read a single variable (becarful, do not forget the "," after the variable name)::

        >>> qv, = dnc.read_var(filename, 'QV')

Or you can read several variables in once::

        >>> th, qv = dnc.read_var(filename, ['TH', 'QV'])


DocStrings
----------

.. automodule:: dypy.netcdf
   :members:
