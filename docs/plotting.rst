plotting
========

This module contains mainly a class Mapfigure, which makes it easier to create Basemap plot and add a method to plots trajectories on top of it. 

Examples
--------

First let's load the class::

        >>> from dypy.plotting import Mapfigure

Then prepare a map you have two main options:

1. with a domain definition::

        >>> domain = [5, 10, 45, 47]
        >>> m = Mapfigure(domain=domain)

2. Or with lon, lat coordinates::

        >>> m = Mapfigure(lon=lon, lat=lat)

In this case, the domain is based on the boundary values of the lon, lat arrays.

The next step is to plot data on the map::

        >>> m.contourf(lon, lat, data)

or, if m was prepared using option 2::

        >>> m.contourf(m.x, m.y, data)

To add the map background (countries and coastlines)::

        >>> m.drawmap(nbrem=1, nbrep=1)

where nbrem and nbrep can be specified to adjust the grid lines

Docstrings
----------

.. automodule:: dypy.plotting
   :members:
   :undoc-members:
