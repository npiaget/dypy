.. _contents:


Welcome to DyPy's documentation!
================================

Contents:

.. toctree::
   :maxdepth: 2

   intro
   lagranto
   netcdf
   plotting
   small_tools
   soundings
   constants
   intergrid

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

