#!/usr/bin/env python
# coding: utf-8

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

__AUTHOR__ = 'Nicolas Piaget'
__AUTHOR_EMAIL__ = 'nicolas.piaget@env.ethz.ch'

readme = open('README.rst').read()  # + '\n\n' + open('CHANGELOG.rst').read()
packages = ['dypy', 'dypy.plotting']
requirements = ['Numpy', 'Scipy', 'netCDF4', 'python-magic',
                'matplotlib', 'pyproj', 'Cartopy', 'fiona']
tests_require = ['pytest']

setup(name='DyPy',
      version='0.1',
      author=__AUTHOR__,
      author_email=__AUTHOR_EMAIL__,
      maintainer=__AUTHOR__,
      maintainer_email=__AUTHOR_EMAIL__,
      description='Collection of tools for atmospheric science',
      long_description=readme,
      packages=packages,
      setup_requires=['pytest-runner>=2.0'],
      install_requires=requirements,
      tests_require=tests_require,
      include_package_data=True,
      classifiers=[
          'Development Status :: 4 - Beta',
          'Programming Language :: Python :: 2.7',
          'Programming Language :: Python :: 3.4',
      ]
      )
